Injection

login : user@mail.com
mdp   : ' or 1=1;--

Result
Login successful!
id: 1 - Username: testuser - Password: password123
id: 2 - Username: admin - Password: adminpass

Fonction PHP utilisée
SQLite3::escapeString(

pour se prémunir des injections SQL

Parameterized Statements =  utiliser des requètes préparées avec l'input en paramètre afin que celui ci soit traité de manière secure
Object Relational Mapping =  utiliser des ORM qui incluent par défaut les requètes préparées
Escaping Inputs = échapper les simple quotes
Sanitizing Inputs = adapter les contrôle de l'input à l'attendu avec des controles regex
