<?php
$db_file = '/var/www/html/database.db';

try {
    $conn = new PDO("sqlite:$db_file");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $user = SQLite3::escapeString($_POST['username']);
    $pass = SQLite3::escapeString($_POST['password']);

    $sql = "SELECT * FROM users WHERE username = '$user' AND password = '$pass'";

    $stmt = $conn->query($sql);

    if ($stmt === false) {
        echo "Error executing the query.<br>";
    } else {
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $rowCount = count($rows);

        if ($rowCount > 0) {
            echo "Login successful!<br>";
            foreach ($rows as $row) {
                echo "id: " . $row["id"]. " - Username: " . $row["username"]. " - Password: " . $row["password"]. "<br>";
            }
        } else {
            echo "Incorrect username or password.";
        }
    }
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}
